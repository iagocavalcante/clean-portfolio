function showMenu() {
    var x = document.getElementById("myLinks");
    if (x.className === "links") {
        x.className += " responsive";
    } else {
        x.className = "links";
    }
}

$('.owl-nome').owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    dots: false,
    navText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

jQuery(function($){

    $( '.nav__burger' ).click(function(){
        $('.nav').toggleClass('nav--opened');
    });
});