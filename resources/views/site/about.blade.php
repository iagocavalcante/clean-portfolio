@extends('layouts.header')
@section('content')
<section class="about">
    <h1 class="title-center">Sobre mim</h1>
    <p class="about">Atuando como desenvolvedor há 4 anos, comecei a despertar o gosto pela programação no segundo
    ano da faculdade, sempre curioso procurei aprender uma linguagem diferente da ensinada lá e assim conheci PHP. Ao passar
    dos anos a curiosidade só aumentava, então comecei a estudar várias linguagens e conhecer um pouco mais sobre elas, 
    despertei um interesse grande pelo movimento maker no mesmo período, fiz algumas coisas usando arduino e raspberry.
    Ingressei no mestrado mas desisti ainda no início por perceber que ali não era meu lugar, mas não foi tempo perdido.
    No mestrado aprendi muito sobre Inteligência Computacional e Mineração de dados, fiquei fascinado por isso, mas ainda
    não consegui aplicar na minha área de atuação. Hoje trabalho como Analista de Sistemas na W3 Automção e Sistemas, empresa
    que atua na área bancária, quem sabe não pinta uma oportunidade de aplicar meus conhecimentos. Entrando um pouco na vida
    pessoal, gosto de jogar bola, ler, viajar, assistir séries e filmes, ficar com a minha familia e sair com os meus melhores amigos. </p>
</section>
<section>
    <h1 class="title-center">A estrada até aqui</h1>
    <div class="grid grid--1of2 career">
        <div class="grid-cell career-right">
            <img class="img-responsive img-circle career-icon-right" src="images/crm.png" alt="">
            <h1 class="title-right">CRM-PA 2013 - 2014</h1>
            <p class="career-right-text career">Início da jornada como desenvolvedor, começando como estagiário,
            atuando tanto como desenvolvedor web como também suporte algumas vezes. Lugar onde comecei a desenvolver
            usando php.</p>
        </div>
        <div class="grid-cell career-left">
            <img class="img-responsive img-circle icon-left" src="images/solux.png" alt="">
            <h1 class="title-left">SOLUX 2015 - 2016</h1>
            <p class="career-left-text career-left-text">Atuando como full stack developer, utilizando PHP, MySQL, 
            JavaScript, HTML e CSS, cuidando também do servidor de aplicação. Era encarregado do desenvolvimento do sistema 
            interno da empresa. </p>
        </div>
        <div class="grid-cell career-right">
            <img class="img-responsive img-circle career-icon-right" src="images/i9.png" alt="">
            <h1 class="title-right">i9Amazon 2016 - 2017/2</h1>
            <p class="career-right-text career">Local onde pude aprimorar todos os meus conhecimentos de programação
            e arquitetura de sistemas, criando um sistema do zero, utilizando JAVA, MariaDb, Maven para o desenvolvimento do 
            sistema carro chefe, e para criação do site e sistema de gerenciamento interno usando Laravel.</p>
        </div>
        <div class="grid-cell career-left">
            <img class="img-responsive img-circle icon-left" src="images/easy.png" alt="">
            <h1 class="title-left">Easy Tec 2016/2 - 2017/1</h1>
            <p class="career-left-text career-left-text">Ajudando na criação de um aplicativo híbrido para área do varejo
            trabalhando efetivamente com Ionic, atuando também em algumas partes da API desenvolvida utilizando .Net Core e SQL Server</p>
        </div>
        <div class="grid-cell career-right">
            <img class="img-responsive img-circle career-icon-right" src="images/vila.png" alt="">
            <h1 class="title-right">Vila do Silício 2016 - Atual</h1>
            <p class="career-right-text career">Um dos idealizadores e autores do blog, tentando ajudar a comunidade com
            alguns tutorias práticos e dicas.</p>
        </div>
        <div class="grid-cell career-left">
            <img class="img-responsive img-circle icon-left" src="images/w3.png" alt="">
            <h1 class="title-left">W3AS 2017 - Atual</h1>
            <p class="career-left-text career-left-text">Trabalhando em novos projetos utilizando conhecimentos de DDD e TDD 
            para a criação de software mais elaborado, fácil de prestar manutenção e que outros dev's possam entender. 
            Utilizando Node.JS para desenvolvimento de API's.</p>
        </div>
    </div>
</section>
@endsection