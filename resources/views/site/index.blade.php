@extends('layouts.header')
@section('content')
<main>

    <section class="portfolio">
        <h1 class="title-center">Portfólio</h1>
        <div class="portfolio-list owl-nome owl-carousel owl-theme">
            <div class="item">
                <img class="img img-responsive" src="images/chopp.jpg" alt="">
            </div>
            <div class="item">
                <img class="img img-responsive" src="images/conceito.jpg" alt="">
            </div>
            <div class="item">
                <img class="img img-responsive" src="images/i9empresa.jpg" alt="">
            </div>
            <div class="item">
                <img class="img img-responsive" src="images/i9gestao.jpg" alt="">
            </div>
            <div class="item">
                <img class="img img-responsive" src="images/i9site.jpg" alt="">
            </div>
            <div class="item">
                <img class="img img-responsive" src="images/vila.jpg" alt="">
            </div>
        </div>
    </section>
    <section>
        <h1 class="title-center">com o que posso ajudar ?! </h1>
        <div class="grid grid--1of2 service">
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/browser.png" alt="">
                <h1 class="service-title">Sites</h1>
                <p class="service-text">Prototipação e criação de sites responsivos, com design
    clean e com o que há de melhor das tecnologias atuais.</p>
            </div>
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/shopping-cart.png" alt="">
                <h1 class="service-title">Mobile</h1>
                <p class="service-text">Criação de aplicativos multiplataformas, com um ótimo
    desempenho e interface simples e bonita.</p>
            </div>
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/archives.png" alt="">
                <h1 class="service-title">Desktop</h1>
                <p class="service-text">Criação de software desktop sobre demanda,
    com excelente desempenho e um bom custo beneficio.;o</p>
            </div>
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/seo.png" alt="">
                <h1 class="service-title">SEO</h1>
                <p class="service-text">Otimizando seu site para ser bem visto pelos sistemas de
    buscas, melhorando o rankeamento do mesmo.</p>
            </div>
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/computer.png" alt="">
                <h1 class="service-title">E-commerce</h1>
                <p class="service-text">Criação de plataformas de venda de forma ágil, rápida,
    segura e customizavel. Integração com serviços de pagamento
    como PagSeguro e PayPal.</p>
            </div>
            <div class="grid-cell service-margin">
                <img class="icon-left-service" src="images/cogwheel.png" alt="">
                <h1 class="service-title">Segurança</h1>
                <p class="service-text">Provendo as melhores técnicas para que os sistemas cons-
    truídos sejam o mais seguro possível.</p>
            </div>
        </div>
    </section>
</main>
@endsection