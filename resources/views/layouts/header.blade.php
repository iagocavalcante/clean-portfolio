<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Iago Cavalcante</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/grid.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    </head>
    <body>
    
    </body>
</html>
<body>
<header>
    <nav id="nav" class="nav" role="navigation">
        <div class="nav__container">

            <!-- Title -->
            <h1 class="nav__title">
                <a href="#" class="nav__title-link">
                    Iago Cavalcante
                    <br>Web/Mobile Developer
                </a>
            </h1>

            <!-- Nav List -->
            <ul class="nav__list" role="menubar">
                <li class="nav__item">
                    <a class="nav__link" href="/">HOME</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="/blog">BLOG</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="/sobre-mim">SOBRE MIM</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link" href="/contato">CONTATO</a>
                </li>
            </ul>

            <!-- Burger -->
            <a href="#" class="nav__burger"></a>

        </div> <!-- __container -->
    </nav> <!-- .nav -->
    {{--<nav class="header-menu">--}}
        {{--<span>--}}
            {{--<a href="#" class="logo">IAGO CAVALCANTE</a>--}}
            {{--<a href="#" class="sublogo">Web/Mobile Developer</a>--}}
        {{--</span>--}}
        {{--<ul class="main-nav">--}}
            {{--<li><a href="/">HOME</a></li>--}}
            {{--<li><a href="/blog">BLOG</a></li>--}}
            {{--<li><a href="/sobre-mim">SOBRE MIM</a></li>--}}
            {{--<li><a href="/contato">CONTATO</a></li>--}}
        {{--</ul>--}}
    {{--</nav>--}}
</header>
@yield('content')
@include('layouts.footer')